<?php
/*
  Plugin Name: SMS Spacer
  Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-spacer
  Description: A spacer section for adding vertical space to your layout
  Author: Roadside Multimedia
  PageLines: true
  Version: 1.1.1
  Section: Spacer
  Class Name: sms_spacer
  Filter: component
  Loading: active
  Text Domain: dms-sms
  Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-spacer
  Bitbucket Branch: master
*/

/**
 * IMPORTANT
 * This tells wordpress to not load the class as DMS will do it later when the main sections API is available.
 * If you want to include PHP earlier like a normal plugin just add it above here.
 */

if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;


class sms_spacer extends PageLinesSection {


  function section_persistent() {
    add_action('wp_head', function(){
      if( ! is_admin() && current_user_can( 'manage_options' ) ){
?>
<style type="text/css">
.drag-drop-editing  .sms-spacer {
  background: url(<?php echo $this->base_url ?>/images/spacing-indicator-icon.png) no-repeat center center;
  background-size: contain;
}
.drag-drop-editing .section-sms-spacer{
  border-top: 1px dashed #35eafd;
  border-bottom: 1px dashed #35eafd;
}
.drag-drop-editing .sms-light .sms-spacer{
    box-shadow: inset 0 -18px 50px -16px rgba(0,0,0,0.1), inset 0 18px 50px -16px rgba(0,0,0,0.1);
}
.drag-drop-editing .sms-dark .sms-spacer{
  box-shadow: inset 0 -18px 50px -16px rgba(255,255,255,0.2), inset 0 18px 50px -16px rgba(255,255,255,0.2);
}
</style>
<?php
      }
    });
  }

  function __construct(){
    parent::__construct();

    $this->height_list = array(
      '10'   => array( 'name' => '10px' ),
      '20'   => array( 'name' => '20px' ),
      '30'   => array( 'name' => '30px' ),
      '40'   => array( 'name' => '40px' ),
      '50'   => array( 'name' => '50px' ),
      '60'   => array( 'name' => '60px' ),
      '70'   => array( 'name' => '70px' ),
      '80'   => array( 'name' => '80px' ),
      '90'   => array( 'name' => '90px' ),
      '100'  => array( 'name' => '100px' ),
      '110'  => array( 'name' => '110px' ),
      '120'  => array( 'name' => '120px' ),
      '130'  => array( 'name' => '130px' ),
      '140'  => array( 'name' => '140px' ),
      '150'  => array( 'name' => '150px' ),
      '160'  => array( 'name' => '160px' ),
      '170'  => array( 'name' => '170px' ),
      '180'  => array( 'name' => '180px' ),
      '190'  => array( 'name' => '190px' ),
      '200'  => array( 'name' => '200px' ),
    );
  }

  function section_opts(){
    $options = array();

    $options[] = array(

      'title' => __( 'Spacer Configuration', 'pagelines' ),
      'type'  => 'multi',
      'opts'  => array(
        array(
          'label'         => __( 'Space in pixels', 'pagelines' ),
          'key'           => 'spacer_value',
          'type'          => 'select',
          'opts'          => $this->height_list,
          'default'       => '50',
        ),
        array(
          'label'         => __( 'Display on small screens?', 'pagelines' ),
          'key'           => 'spacer_display_small_screen',
          'type'          => 'check',
          'default'       => true,
        ),
      )

    );

    return $options;
  }
  function section_template(){

    global $pldraft;
    $edit = false;
    if( is_object( $pldraft ) && 'draft' == $pldraft->mode )
      $edit = true;

    $set_height = ( $this->opt('spacer_value') * 0.1 ) ?: 5; // 5em / 50px default
    $height = sprintf('height: %sem', $set_height);
    $height_sync_data = (pl_draft_mode()) ? 'data-sync="spacer_value" data-sync-mode="css" data-sync-target="height" data-sync-post="px"' : '';

    $display_small_screen = ($this->opt('spacer_display_small_screen')) ? '' : ' hidden-phone';
    $output = sprintf('<div class="sms-spacer %2$s" %3$s style="%1$s"></div>', $height, $display_small_screen, $height_sync_data);
    echo $output;
  }

}
