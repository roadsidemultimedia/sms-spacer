### Changelog

#### 1.1.1 [11/25/2014] {WIP}

 * Fixed duplicate CSS issue that would cause numerous copies of the admin indicator CSS to output to the page when logged in.
 * Fixed default size. Instead of defaulting to 200px, it now defaults to 50px.

#### 1.1.0 [08/13/2014]

 * Spacing updates in realtime (doesn't resize for mobile after a live update)
 * Spacing size is relative to the html size, so it will resize on smaller screens along with the other typography.
 * Visual indicator style update
 * Optimized CSS (only outputs front end indicator css if logged in with editor priviledges)
 * Spacing in increments of 10 instead of 1
 

#### 1.0.1

 * Option to display on small screens actually works now.
 
#### 1.0.0
 
 * Initial release